// productos.js

// Obtén una referencia a la base de datos
var database = firebase.database();

// Escucha cambios en la colección "productos"
database.ref('productos').on('value', function(snapshot) {
  var productos = snapshot.val();
  mostrarProductos(productos);
});

function mostrarProductos(productos) {
  var productosContainer = document.getElementById('productosContainer');
  productosContainer.innerHTML = ""; // Limpiar contenido anterior

  for (var key in productos) {
    var producto = productos[key];
    var productoHTML = `
      <section class="container product">
        <img src="${producto.imagen}" alt="${producto.nombre}" height="240" width="150">
        <p>Marca: ${producto.marca}</p>
        <p>Forma del artículo: ${producto.forma}</p>
        <!-- Agrega más campos según la estructura de tus productos -->
      </section>
    `;
    productosContainer.innerHTML += productoHTML;
  }
}
