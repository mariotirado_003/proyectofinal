// Importa el módulo de autenticación de Firebase
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// Tu configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyC8as3a4F_ZYzxqHE1vVpTyKjdhSygIat8",
    authDomain: "proyectofinal-2d592.firebaseapp.com",
    databaseURL: "https://proyectofinal-2d592-default-rtdb.firebaseio.com/",
    projectId: "proyectofinal-2d592",
    storageBucket: "proyectofinal-2d592.appspot.com",
    messagingSenderId: "317889314373",
    appId: "1:317889314373:web:25070d1de8d0ef22282d2c"
};

// Inicializa Firebase
const app = initializeApp(firebaseConfig);

// Obtiene la referencia al elemento del formulario
const btnEnviar = document.getElementById('btnEnviar');


// Agrega un evento de escucha para el envío del formulario
btnEnviar.addEventListener("click", function(event) {
  event.preventDefault(); // Evita que el formulario se envíe de forma convencional

  // Obtiene los valores del correo electrónico y la contraseña desde el formulario
  const email = document.getElementById('txtEmail').value;
  const password = document.getElementById('txtPassword').value;

  // Obtiene la instancia de autenticación de Firebase
  const auth = getAuth();

  // Intenta autenticar al usuario con el correo electrónico y la contraseña proporcionados
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Usuario autenticado correctamente
      const user = userCredential.user;
      alert(`Usuario autenticado${user.email}`);

      // Redirige al usuario a la página de inicio, por ejemplo:
      window.location.href = "/html/personal.html";
    })
    .catch((error) => {
      // Error durante la autenticación
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(`Error durante la autenticación: ${errorMessage}`);
   });
   
});