// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyC8as3a4F_ZYzxqHE1vVpTyKjdhSygIat8",
  authDomain: "proyectofinal-2d592.firebaseapp.com",
  databaseURL: "https://proyectofinal-2d592-default-rtdb.firebaseio.com/",
  projectId: "proyectofinal-2d592",
  storageBucket: "proyectofinal-2d592.appspot.com",
  messagingSenderId: "317889314373",
  appId: "1:317889314373:web:25070d1de8d0ef22282d2c"
};

// Inicializa Firebase
const app = firebase.initializeApp(firebaseConfig);

// Función para registrar un nuevo usuario
function registerUser() {
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    const auth = firebase.auth();
    createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Usuario registrado con éxito
            const user = userCredential.user;
            console.log('Usuario registrado:', user);
            // Puedes agregar lógica adicional aquí, como redireccionar a otra página
        })
        .catch((error) => {
            // Handle Errors here.
            const errorCode = error.code;
            const errorMessage = error.message;
            console.error('Error al registrar usuario:', errorCode, errorMessage);
        });
}