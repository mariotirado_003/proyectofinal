productosRef.on('value', (snapshot) => {
    // Limpiar el contenedor de productos antes de agregar nuevos productos
    document.getElementById('contenedor').innerHTML = '';

    snapshot.forEach((productoSnapshot) => {
        const producto = productoSnapshot.val();

        // Verificar si el producto está marcado como eliminado y no tiene status "Deshabilitado"
        if (!producto.eliminado && producto.status !== "Deshabilitado") {
            // Crear elementos para mostrar el producto
            const productoDiv = document.createElement('div');
            productoDiv.className = 'producto'; // Asegúrate de aplicar la clase 'producto'

            // Mostrar la imagen del producto
            const imagen = document.createElement('img');
            imagen.src = producto.url; // La URL de la imagen obtenida de Firebase
            imagen.alt = producto.nombre; // Nombre del producto como texto alternativo
            imagen.className = 'producto-imagen'; // Asegúrate de aplicar la clase 'producto-imagen'
            productoDiv.appendChild(imagen);

            // Mostrar otros datos del producto
            const nombreParrafo = document.createElement('p');
            nombreParrafo.innerHTML = `<strong>Nombre:</strong> ${producto.nombre}`;
            productoDiv.appendChild(nombreParrafo);

            const precioParrafo = document.createElement('p');
            precioParrafo.innerHTML = `<strong>Precio:</strong> $${producto.precio}`;
            productoDiv.appendChild(precioParrafo);

            const codigoParrafo = document.createElement('p');
            codigoParrafo.innerHTML = `<strong>Código:</strong> ${producto.codigo}`;
            productoDiv.appendChild(codigoParrafo);

            const statusParrafo = document.createElement('p');
            statusParrafo.innerHTML = `<strong>Estado:</strong> ${producto.status}`;
            productoDiv.appendChild(statusParrafo);

            // Agregar el producto al contenedor de productos
            document.getElementById('contenedor').appendChild(productoDiv);
        }
    });
});

  

